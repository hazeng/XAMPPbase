# Declare the package name:
atlas_subdir( XAMPPbase )

if($ENV{AtlasVersion} MATCHES "^21")
  # Declare the package's dependencies:
  atlas_depends_on_subdirs(
   PUBLIC
   Control/AthToolSupport/AsgTools  
   Control/AthenaBaseComps 
   Control/AthAnalysisBaseComps
   GaudiKernel
   Event/xAOD/xAODCore
   Event/xAOD/xAODEgamma
   Event/xAOD/xAODEventInfo
   Event/xAOD/xAODJet
   Event/xAOD/xAODMissingET
   Event/xAOD/xAODTrigMissingET
   Event/xAOD/xAODMuon
   Event/xAOD/xAODTau
   Event/xAOD/xAODTracking
   Event/xAOD/xAODTruth
   Event/xAOD/xAODParticleEvent 
   Event/xAOD/xAODLuminosity    
   PhysicsAnalysis/AnalysisCommon/PATInterfaces
   PhysicsAnalysis/AnalysisCommon/PMGTools
   PhysicsAnalysis/MCTruthClassifier
   PhysicsAnalysis/SUSYPhys/SUSYTools    
   Trigger/TrigAnalysis/TrigDecisionTool
   CalcGenericMT2
   IFFTruthClassifier
   PhysicsAnalysis/D3PDTools/AnaAlgorithm
   PRIVATE   
   Control/xAODRootAccess
   Control/AthContainers
   Event/EventPrimitives
   Event/FourMomUtils
   Event/xAOD/xAODBTagging
   Event/xAOD/xAODBase
   Event/xAOD/xAODCaloEvent
   Event/xAOD/xAODCutFlow
   Event/xAOD/xAODPrimitives
   Reconstruction/MET/METInterface
   PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces
   PhysicsAnalysis/Interfaces/MuonAnalysisInterfaces
   PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces
   PhysicsAnalysis/Interfaces/JetAnalysisInterfaces
   PhysicsAnalysis/Interfaces/FTagAnalysisInterfaces
   PhysicsAnalysis/TauID/TauAnalysisTools
   PhysicsAnalysis/AnalysisCommon/IsolationSelection
   Reconstruction/Jet/JetJvtEfficiency
   Reconstruction/tauRecTools
   Tools/PathResolver
   Trigger/TrigAnalysis/TriggerMatchingTool
   Trigger/TrigConfiguration/TrigConfInterfaces
   Trigger/TrigConfiguration/TrigConfxAOD
  )
endif()

# External dependencies:
find_package( ROOT COMPONENTS Core Tree RIO Hist Physics )

# Libraries in the package:
atlas_add_library( XAMPPbaseLib
   XAMPPbase/*.h Root/*.cxx
   PUBLIC_HEADERS XAMPPbase
   INCLUDE_DIRS  ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} AsgTools xAODCore xAODEgamma xAODEventInfo xAODRootAccess
   xAODJet xAODMissingET xAODMuon xAODTau xAODTracking xAODTruth xAODParticleEvent xAODLuminosity SUSYToolsLib xAODCutFlow CalcGenericMT2Lib
      
   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} 
   AssociationUtilsLib PATInterfaces TrigDecisionToolLib PMGToolsLib
   MCTruthClassifierLib JetSubStructureUtils
   AthContainers EventPrimitives FourMomUtils xAODBTagging xAODBase
   xAODPrimitives IsolationSelectionLib PileupReweightingLib  
   TauAnalysisToolsLib IFFTruthClassifierLib 
   JetUncertaintiesLib JetMomentToolsLib METInterface METUtilitiesLib
   PathResolver TriggerMatchingToolLib TrigConfInterfaces TrigConfxAODLib
   xAODTrigMissingET xAODMetaData AsgAnalysisInterfaces MuonAnalysisInterfacesLib 
   PRIVATE_LINK_LIBRARIES xAODTrigger PathResolver AnaAlgorithmLib )


atlas_add_component( XAMPPbase
      src/*.h src/*.cxx src/components/*.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} XAMPPbaseLib AthAnalysisBaseComps  AthenaBaseComps )


atlas_add_dictionary( XAMPPbaseDict
   XAMPPbase/XAMPPbaseDict.h
   XAMPPbase/selection.xml
   LINK_LIBRARIES XAMPPbaseLib )

atlas_add_executable( SlimPRWFile
   util/SlimPRWFile.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} xAODRootAccess XAMPPbaseLib )



# Install files from the package:
atlas_install_data( data/* )
atlas_install_data( scripts/*.sh )

atlas_install_python_modules( python/*.py )
atlas_install_joboptions( share/*.py )

